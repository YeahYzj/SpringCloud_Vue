/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80032 (8.0.32)
 Source Host           : 127.0.0.1:3306
 Source Schema         : cloud_admin_v1

 Target Server Type    : MySQL
 Target Server Version : 80032 (8.0.32)
 File Encoding         : 65001

 Date: 01/01/2024 23:55:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_client
-- ----------------------------
DROP TABLE IF EXISTS `auth_client`;
CREATE TABLE `auth_client`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '服务编码',
  `secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '服务密钥',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '服务名',
  `locked` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否锁定',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `crt_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人姓名',
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人',
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新姓名',
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新主机',
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_client
-- ----------------------------
INSERT INTO `auth_client` VALUES (1, 'gateway-module', '123456', 'gateway-module', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (3, 'admin-module', '123456', 'admin-module', '0', '', NULL, NULL, NULL, NULL, '2017-07-06 21:42:17', '1', '管理员', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (6, 'auth-module', '123456', 'auth-module', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (11, 'ace-config', 'fXHsssa2', 'ace-config', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (13, 'ace-template', 'bZf8yvj8', 'ace-template', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (14, 'ace-trace', 'wKTl6GGE', 'ace-trace', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (15, 'ace-monitor', 'eEQBUcnW', 'ace-monitor', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (16, 'ace-gateway', 'PHK3CLfo', 'ace-gateway', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES (17, 'ace-sample', '123456', 'ace-sample', '0', 'ace-sample', '2020-09-05 03:33:35', '1', 'admin', '0:0:0:0:0:0:0:1', '2020-09-05 03:33:35', '1', 'admin', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for auth_client_service
-- ----------------------------
DROP TABLE IF EXISTS `auth_client_service`;
CREATE TABLE `auth_client_service`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_client_service
-- ----------------------------
INSERT INTO `auth_client_service` VALUES (21, '4', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (23, '3', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (41, '3', '1', NULL, '2017-12-31 08:58:03', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (42, '6', '1', NULL, '2017-12-31 08:58:03', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for base_element
-- ----------------------------
DROP TABLE IF EXISTS `base_element`;
CREATE TABLE `base_element`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源编码',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源类型',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源名称',
  `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源路径',
  `menu_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源关联菜单',
  `parent_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `path` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源树状检索路径',
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源请求类型',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_element
-- ----------------------------
INSERT INTO `base_element` VALUES (3, 'userManager:btn_add', 'button', '新增', '/admin/user', '1', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (4, 'userManager:btn_edit', 'button', '编辑', '/admin/user/{*}', '1', NULL, NULL, 'PUT', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (5, 'userManager:btn_del', 'button', '删除', '/admin/user/{*}', '1', NULL, NULL, 'DELETE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (9, 'menuManager:element', 'uri', '按钮页面', '/admin/element', '6', NULL, NULL, 'GET', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (10, 'menuManager:btn_add', 'button', '新增', '/admin/menu/{*}', '6', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (11, 'menuManager:btn_edit', 'button', '编辑', '/admin/menu/{*}', '6', '', '', 'PUT', '', '2017-06-24 00:00:00', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES (12, 'menuManager:btn_del', 'button', '删除', '/admin/menu/{*}', '6', '', '', 'DELETE', '', '2017-06-24 00:00:00', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES (13, 'menuManager:btn_element_add', 'button', '新增元素', '/admin/element', '6', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (14, 'menuManager:btn_element_edit', 'button', '编辑元素', '/admin/element/{*}', '6', NULL, NULL, 'PUT', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (15, 'menuManager:btn_element_del', 'button', '删除元素', '/admin/element/{*}', '6', NULL, NULL, 'DELETE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (16, 'groupManager:btn_add', 'button', '新增', '/admin/group', '7', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (17, 'groupManager:btn_edit', 'button', '编辑', '/admin/group/{*}', '7', NULL, NULL, 'PUT', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (18, 'groupManager:btn_del', 'button', '删除', '/admin/group/{*}', '7', NULL, NULL, 'DELETE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (19, 'groupManager:btn_userManager', 'button', '分配用户', '/admin/group/{*}/user', '7', NULL, NULL, 'PUT', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (20, 'groupManager:btn_resourceManager', 'button', '分配权限', '/admin/group/{*}/authority', '7', NULL, NULL, 'GET', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (21, 'groupManager:menu', 'uri', '分配菜单', '/admin/group/{*}/authority/menu', '7', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (22, 'groupManager:element', 'uri', '分配资源', '/admin/group/{*}/authority/element', '7', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (23, 'userManager:view', 'uri', '查看', '/admin/user/{*}', '1', '', '', 'GET', '', '2017-06-26 00:00:00', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES (24, 'menuManager:view', 'uri', '查看', '/admin/menu/{*}', '6', '', '', 'GET', '', '2017-06-26 00:00:00', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `base_element` VALUES (27, 'menuManager:element_view', 'uri', '查看', '/admin/element/{*}', '6', NULL, NULL, 'GET', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (28, 'groupManager:view', 'uri', '查看', '/admin/group/{*}', '7', NULL, NULL, 'GET', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (32, 'groupTypeManager:view', 'uri', '查看', '/admin/groupType/{*}', '8', NULL, NULL, 'GET', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (33, 'groupTypeManager:btn_add', 'button', '新增', '/admin/groupType', '8', NULL, NULL, 'POST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (34, 'groupTypeManager:btn_edit', 'button', '编辑', '/admin/groupType/{*}', '8', NULL, NULL, 'PUT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (35, 'groupTypeManager:btn_del', 'button', '删除', '/admin/groupType/{*}', '8', NULL, NULL, 'DELETE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (41, 'gateLogManager:view', 'button', '查看', '/admin/gateLog', '27', NULL, NULL, 'GET', '', '2017-07-01 00:00:00', '1', 'admin', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (42, 'serviceManager:view', 'URI', '查看', '/auth/service/{*}', '30', NULL, NULL, 'GET', NULL, '2017-12-26 20:17:42', '1', 'Mr.AG', '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (43, 'serviceManager:btn_add', 'button', '新增', '/auth/service', '30', NULL, NULL, 'POST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (44, 'serviceManager:btn_edit', 'button', '编辑', '/auth/service/{*}', '30', NULL, NULL, 'PUT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (45, 'serviceManager:btn_del', 'button', '删除', '/auth/service/{*}', '30', NULL, NULL, 'DELETE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (46, 'serviceManager:btn_clientManager', 'button', '服务授权', '/auth/service/{*}/client', '30', NULL, NULL, 'POST', NULL, '2017-12-30 16:32:48', '1', 'Mr.AG', '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (47, 'onlineManager:btn_forceLogout', 'button', '强退', '/auth/online/{*}', '39', NULL, NULL, 'DELETE', NULL, '2020-08-16 03:37:02', '1', 'admin', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (48, 'resumeManager:btn_add', 'button', '新增', '/admin/resume/', '1', NULL, NULL, 'POST', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (49, 'resumeManager:btn_edit', 'button', '编辑', '/admin/resume/{*}', '1', NULL, NULL, 'PUT', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (50, 'resumeManager:btn_del', 'button', '删除', '/admin/resume/{*}', '1', NULL, NULL, 'DELETE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_element` VALUES (51, 'resumeManager:view', 'uri', '查看', '/admin/resume/{*}', '1', '', '', 'GET', '', '2017-06-26 00:00:00', '', '', '', '', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for base_group
-- ----------------------------
DROP TABLE IF EXISTS `base_group`;
CREATE TABLE `base_group`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色编码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `parent_id` int NOT NULL COMMENT '上级节点',
  `path` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '树状关系',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型',
  `group_type` int NOT NULL COMMENT '角色组类型',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_time` datetime NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_group
-- ----------------------------
INSERT INTO `base_group` VALUES (1, 'adminRole', '管理员', -1, '/adminRole', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group` VALUES (3, 'testGroup', '体验组', -1, '/testGroup', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group` VALUES (4, 'SystemRole', '系统管理员', 3, '/testGroup/visitorRole', NULL, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group` VALUES (5, 'hrRole', '人力资源主管', -1, '/company/hrRole', NULL, 2, '', NULL, NULL, NULL, NULL, '2020-07-26 07:58:58', '1', 'admin', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group` VALUES (6, 'userRole', '普通用户', 6, '/company/financeDepart', NULL, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group` VALUES (8, 'hrDepart', '人力资源部', 6, '/company/hrDepart', NULL, 2, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group` VALUES (9, 'blogAdmin', '博客管理员', -1, '/blogAdmin', NULL, 1, '', '2017-07-16 16:59:30', '1', 'Mr.AG', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for base_group_leader
-- ----------------------------
DROP TABLE IF EXISTS `base_group_leader`;
CREATE TABLE `base_group_leader`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_time` datetime NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_group_leader
-- ----------------------------

-- ----------------------------
-- Table structure for base_group_member
-- ----------------------------
DROP TABLE IF EXISTS `base_group_member`;
CREATE TABLE `base_group_member`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_time` datetime NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_group_member
-- ----------------------------
INSERT INTO `base_group_member` VALUES (12, '5', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (17, '4', '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (24, '6', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (25, '6', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (26, '5', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (27, '5', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (28, '4', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (29, '6', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (30, '5', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_member` VALUES (31, '4', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for base_group_type
-- ----------------------------
DROP TABLE IF EXISTS `base_group_type`;
CREATE TABLE `base_group_type`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '编码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `crt_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人ID',
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新主机',
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_group_type
-- ----------------------------
INSERT INTO `base_group_type` VALUES (1, 'role', '角色类型', 'role', NULL, NULL, NULL, NULL, '2017-08-25 17:52:37', '1', 'Mr.AG', '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_type` VALUES (2, 'depart', '部门类型', NULL, NULL, NULL, NULL, NULL, '2017-08-25 17:52:43', '1', 'Mr.AG', '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_group_type` VALUES (3, 'free', '自定义类型', 'sadf', NULL, NULL, NULL, NULL, '2017-08-26 08:22:25', '1', 'Mr.AG', '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路径编码',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `parent_id` int NOT NULL COMMENT '父级节点',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源路径',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `type` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `order_num` int NOT NULL DEFAULT 0 COMMENT '排序',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单上下级关系',
  `enabled` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '启用禁用',
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_time` datetime NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_menu
-- ----------------------------
INSERT INTO `base_menu` VALUES (1, 'userManager', '用户管理', 5, '/admin/user', 'user', 'menu', 0, '', '/adminSys/baseManager/userManager', NULL, NULL, NULL, NULL, NULL, '2020-07-18 10:40:37', '1', 'admin', '0:0:0:0:0:0:0:1', 'pages/admin/user/index', '{\"title\": \"角色管理\",\"cache\": true}', 'userManager', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_menu` VALUES (5, 'baseManager', '基本信息管理', 13, '/admin', 'list', 'dirt', 0, '', '/adminSys/baseManager', NULL, NULL, NULL, NULL, NULL, '2020-07-18 14:18:35', '1', 'admin', '0:0:0:0:0:0:0:1', 'layout/header-aside/layout', '{\"title\": \"角色管理\",\"cache\": true}', 'layoutHeaderAside', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_menu` VALUES (13, 'adminSys', '用户管理系统', -1, '/admin', 'chrome', 'sys', 0, '', '/adminSys', NULL, NULL, NULL, NULL, NULL, '2020-07-18 10:42:08', '1', 'admin', '0:0:0:0:0:0:0:1', '', '{\"title\": \"角色管理\",\"cache\": true}', '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_menu` VALUES (27, 'gateLogManager', '操作日志', 5, '/admin/gateLog', 'file-archive-o', 'menu', 0, '', '/adminSys/baseManager/gateLogManager', NULL, '2017-07-01 00:00:00', '1', 'admin', '0:0:0:0:0:0:0:1', '2020-07-18 14:51:30', '1', 'admin', '0:0:0:0:0:0:0:1', 'pages/admin/gateLog/index', '{\"title\": \"角色管理\",\"cache\": true}', 'gateLogManager', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_menu` VALUES (40, 'resumeManager', '履历信息', 5, '/admin/resume', 'line-chart', 'menu', 0, '', '/adminSys/baseManager/resumeManager', NULL, NULL, NULL, NULL, NULL, '2020-07-18 10:40:37', '1', 'admin', '0:0:0:0:0:0:0:1', 'pages/admin/resume/index', '{\"title\": \"角色管理\",\"cache\": true}', 'resumeManager', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for base_resource_authority
-- ----------------------------
DROP TABLE IF EXISTS `base_resource_authority`;
CREATE TABLE `base_resource_authority`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `authority_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色ID',
  `authority_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色类型',
  `resource_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源ID',
  `resource_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源类型',
  `parent_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `path` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1161 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_resource_authority
-- ----------------------------
INSERT INTO `base_resource_authority` VALUES (287, '1', 'group', '5', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (288, '1', 'group', '9', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (289, '1', 'group', '10', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (290, '1', 'group', '11', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (291, '1', 'group', '12', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (294, '1', 'group', '5', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (295, '1', 'group', '9', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (296, '1', 'group', '10', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (297, '1', 'group', '11', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (298, '1', 'group', '12', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (299, '1', 'group', '9', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (300, '1', 'group', '12', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (301, '1', 'group', '10', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (302, '1', 'group', '11', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (303, '1', 'group', '13', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (304, '1', 'group', '14', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (305, '1', 'group', '15', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (306, '1', 'group', '10', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (307, '1', 'group', '11', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (308, '1', 'group', '12', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (309, '1', 'group', '13', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (310, '1', 'group', '14', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (311, '1', 'group', '9', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (312, '1', 'group', '15', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (313, '1', 'group', '16', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (314, '1', 'group', '17', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (315, '1', 'group', '18', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (317, '1', 'group', '20', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (318, '1', 'group', '21', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (319, '1', 'group', '22', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (371, '1', 'group', '23', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (372, '1', 'group', '24', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (373, '1', 'group', '27', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (374, '1', 'group', '28', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (375, '1', 'group', '23', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (378, '1', 'group', '5', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (379, '1', 'group', '9', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (380, '1', 'group', '11', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (381, '1', 'group', '14', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (382, '1', 'group', '13', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (383, '1', 'group', '15', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (384, '1', 'group', '12', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (385, '1', 'group', '24', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (386, '1', 'group', '10', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (387, '1', 'group', '27', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (388, '1', 'group', '16', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (389, '1', 'group', '18', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (390, '1', 'group', '17', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (392, '1', 'group', '20', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (393, '1', 'group', '28', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (394, '1', 'group', '22', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (395, '1', 'group', '21', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (401, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (402, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (403, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (421, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (422, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (436, '1', 'group', '32', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (437, '1', 'group', '33', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (438, '1', 'group', '34', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (439, '1', 'group', '35', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (464, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (465, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (466, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (467, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (468, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (469, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (470, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (471, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (472, '1', 'group', '40', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (492, '1', 'group', '30', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (493, '1', 'group', '31', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (494, '1', 'group', '40', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (666, '1', 'group', '41', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (689, '1', 'group', '43', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (691, '1', 'group', '44', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (710, '9', 'group', '42', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (711, '9', 'group', '43', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (712, '9', 'group', '44', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (713, '9', 'group', '45', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (718, '9', 'group', '42', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (719, '9', 'group', '44', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (720, '9', 'group', '45', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (721, '9', 'group', '43', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (722, '1', 'group', '41', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (749, '10', 'group', '13', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (750, '10', 'group', '14', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (751, '10', 'group', '-1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (752, '10', 'group', '5', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (753, '10', 'group', '6', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (754, '10', 'group', '17', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (755, '10', 'group', '20', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (774, '1', 'group', '3', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (775, '1', 'group', '4', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (812, '1', 'group', '19', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (924, '1', 'group', '42', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (945, '1', 'group', '45', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (956, '1', 'group', '46', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (981, '9', 'group', '23', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (982, '9', 'group', '1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (983, '9', 'group', '13', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (984, '9', 'group', '-1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (985, '9', 'group', '5', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1106, '1', 'group', '47', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1107, '1', 'group', '33', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1108, '1', 'group', '34', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1109, '1', 'group', '13', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1110, '1', 'group', '35', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1111, '1', 'group', '36', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1112, '1', 'group', '37', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1113, '1', 'group', '-1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1114, '1', 'group', '27', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1115, '1', 'group', '38', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1116, '1', 'group', '39', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1117, '1', 'group', '29', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1118, '1', 'group', '1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1119, '1', 'group', '5', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1120, '1', 'group', '6', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1121, '1', 'group', '7', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1122, '1', 'group', '8', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1123, '1', 'group', '30', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1124, '1', 'group', '31', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1125, '1', 'group', '32', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1126, '1', 'group', '40', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1127, '1', 'group', '48', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1128, '1', 'group', '49', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1129, '1', 'group', '50', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1130, '1', 'group', '51', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1131, '4', 'group', '27', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1132, '4', 'group', '13', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1133, '4', 'group', '5', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1134, '5', 'group', '13', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1135, '5', 'group', '5', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1136, '5', 'group', '1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1137, '5', 'group', '3', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1138, '5', 'group', '4', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1139, '5', 'group', '5', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1140, '5', 'group', '23', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1141, '5', 'group', '48', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1142, '5', 'group', '49', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1143, '5', 'group', '50', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1144, '5', 'group', '51', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1146, '5', 'group', '40', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1147, '6', 'group', '13', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1149, '6', 'group', '1', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1150, '6', 'group', '40', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1152, '6', 'group', '4', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1154, '6', 'group', '23', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1155, '6', 'group', '48', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1156, '6', 'group', '49', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1158, '6', 'group', '51', 'button', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_resource_authority` VALUES (1159, '6', 'group', '5', 'menu', '-1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for base_resume
-- ----------------------------
DROP TABLE IF EXISTS `base_resume`;
CREATE TABLE `base_resume`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `birth_date` date NULL DEFAULT NULL,
  `contact_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `education_degree` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alma_mater` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `major` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `graduation_date` date NULL DEFAULT NULL,
  `job_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `job_responsibility` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `employment_period` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `skills` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `professional_certificate` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `training_experience` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `project_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `project_achievement` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `awards` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `honors` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hobbies` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `social_media_accounts` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `self_evaluation` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_resume
-- ----------------------------
INSERT INTO `base_resume` VALUES (1, 'John Doe', '1990-01-01', '123-456-7890', '123 Main St, City', '博士', 'University XYZ', 'Computer Science', '2012-05-15', 'Software Engineer', 'Tech Solutions Inc.', 'Developing and maintaining software applications', '2012-2015', 'Java, JavaScript, SQL', 'Certified Java Developer', 'Completed advanced Java training course', 'Online Shopping Platform', 'Lead Developer', 'Developed a scalable and secure online shopping platform', 'Increased user engagement by 30%', 'Employee of the Month', 'Dean\'s List', 'Reading, Traveling', '@john.doe', 'Highly motivated software engineer with a passion for innovation.', NULL);
INSERT INTO `base_resume` VALUES (2, 'John Doe', '1990-01-01', '123-456-7890', '123 Main St, City', 'Bachelor\'s Degree', 'University XYZ', 'Computer Science', '2012-05-15', 'Software Engineer', 'Tech Solutions Inc.', 'Developing and maintaining software applications', '2012-2015', 'Java, JavaScript, SQL', 'Certified Java Developer', 'Completed advanced Java training course', 'Online Shopping Platform', 'Lead Developer', 'Developed a scalable and secure online shopping platform', 'Increased user engagement by 30%', 'Employee of the Month', 'Dean\'s List', 'Reading, Traveling', '@john.doe', 'Highly motivated software engineer with a passion for innovation.', NULL);
INSERT INTO `base_resume` VALUES (3, '徐必成', '2024-01-01', '110', '地球村', '硕士', '学院', '软件', '2024-01-01', 'Java开发工程师', 'Java技术有限公司', '负责Java微服务开发', '2024-2026', 'Java、MySQL、Oracle、Python、Vue、ElasticSearch', '软件高级证书', '无', '代号：9871', '总监', '保密', '保密', '特等奖', '终生成就奖', '看美女', 'yeach.yzj@gmail.com', 'good12323', '23');

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `birthday` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `mobile_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `tel_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_time` datetime NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_time` datetime NULL DEFAULT NULL,
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `is_logout` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES (1, 'admin', '$2a$12$UTNHPaR/fO7.QettAM/ly.jZObXSuyZzGJo9w2O/UQKl0yWLMAW/q', 'admin', '', NULL, '', NULL, '', '男', NULL, NULL, '123', NULL, NULL, NULL, NULL, '2024-01-01 21:28:10', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `base_user` VALUES (2, 'sys', '$2a$12$UTNHPaR/fO7.QettAM/ly.jZObXSuyZzGJo9w2O/UQKl0yWLMAW/q', '系统管理员', '', NULL, '', NULL, '', '男', NULL, NULL, '', NULL, NULL, NULL, NULL, '2024-01-01 16:09:33', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `base_user` VALUES (17, 'plus', '$2a$12$SPsWp6Hn1984Xnx2UB2rPuxyik6xMsAJR7Yq/VLiEAdISXcTiwU3G', '小黑', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-01 20:49:30', 'null', 'null', 'null', '2024-01-01 23:07:44', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `base_user` VALUES (18, 'kic', '$2a$12$X7n94ZnXOaBKQnBMMWNxZertHe6oN4ECSP8WgPtsogGfkyftIILrS', '阿狸', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-01 20:50:53', 'null', 'null', 'null', '2024-01-01 21:45:16', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `base_user` VALUES (19, 'yeah', '$2a$12$c12x17OnuJtjGO9TYdIxQe6ozV11paTMjqcIWVfQQRhhnWh9yPcia', '小红', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-01 21:42:15', 'null', 'null', 'null', '2024-01-01 21:42:15', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `base_user` VALUES (20, 'dhy', '$2a$12$9.sIQV5HGA9ebqfSZbDzR.c4jl5bHTjDX2mo.Gx8iernf0HHlp.vS', '大黄鸭', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-01 23:07:04', 'null', 'null', 'null', '2024-01-01 23:07:04', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `base_user` VALUES (21, 'sysRole', '$2a$12$2VjbAbLOrjI6LSf5DM4ivecjxwCew2e0tfnphyEbcjEHLAohc/IuG', '系统管理员', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-01 23:09:17', 'null', 'null', 'null', '2024-01-01 23:09:17', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `base_user` VALUES (24, 'xiaohei', '$2a$12$qz8move/tiqDubtdUYiAtOg2HD.nV/RFwrMC.f20UnzVJRCgL5LrS', '小黑', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-01 23:41:06', 'null', 'null', 'null', '2024-01-01 23:41:06', 'null', 'null', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for gate_log
-- ----------------------------
DROP TABLE IF EXISTS `gate_log`;
CREATE TABLE `gate_log`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '序号',
  `menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单',
  `opt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作',
  `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资源路径',
  `crt_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作人ID',
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作人',
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作主机',
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 863 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gate_log
-- ----------------------------
INSERT INTO `gate_log` VALUES (1, '用户管理', '编辑', '/admin/user/{*}', '2020-08-23 08:20:24', '1', 'Mr.AG', '/0:0:0:0:0:0:0:1:58035', '{\"id\":4,\"username\":\"blog\",\"name\":\"Mr.AG(博主)\",\"birthday\":\"\",\"mobilePhone\":\"\",\"email\":\"\",\"sex\":\"男\",\"description\":\"123\\n456\",\"updTime\":\"2020-08-23 16:17:13\",\"updUser\":\"1\",\"updName\":\"admin\",\"updHost\":\"0:0:0:0:0:0:0:1\"}');
INSERT INTO `gate_log` VALUES (2, '用户管理', '编辑', '/admin/user/{*}', '2020-08-23 08:28:41', '1', 'Mr.AG', '/0:0:0:0:0:0:0:1:60395', '{\"id\":4,\"username\":\"blog\",\"name\":\"Mr.AG(博主)\",\"birthday\":\"\",\"mobilePhone\":\"\",\"email\":\"\",\"sex\":\"男\",\"description\":\"123\\n456\\n789\",\"updTime\":\"2020-08-23 16:20:24\",\"updUser\":\"1\",\"updName\":\"admin\",\"updHost\":\"0:0:0:0:0:0:0:1\"}');
INSERT INTO `gate_log` VALUES (3, '服务管理', '新增', '/auth/service', '2020-09-05 03:33:35', '1', 'Mr.AG', '/0:0:0:0:0:0:0:1:54224', '{\"name\":\"ace-sample\",\"sex\":\"男\",\"description\":\"ace-sample\",\"code\":\"ace-sample\",\"secret\":\"123456\"}');
INSERT INTO `gate_log` VALUES (19, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (20, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (21, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (22, '1', 'PUT', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (23, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (24, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (25, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (26, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (27, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (28, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (29, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (30, '1', 'PUT', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (31, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (32, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (33, '1', 'PUT', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (34, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (35, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (36, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (37, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (38, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (39, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (40, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (41, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (42, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (43, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (44, '4', 'GET', '/user/4', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (45, '4', 'PUT', '/user/4', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (46, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (47, '4', 'DELETE', '/user/4', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (48, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (49, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (50, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (51, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (52, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (53, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (54, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (55, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (56, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (57, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (58, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (59, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (60, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (61, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (62, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (63, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (64, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (65, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (66, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (67, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (68, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (69, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (70, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (71, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (72, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (73, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (74, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (75, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (76, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (77, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (78, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (79, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (80, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (81, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (82, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (83, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (84, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (85, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (86, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (87, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (88, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (89, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (90, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (91, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (92, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (93, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (94, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (95, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (96, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (97, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (98, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (99, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (100, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (101, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (102, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (103, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (104, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (105, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (106, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (107, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (108, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (109, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (110, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (111, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (112, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (113, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (114, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (115, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (116, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (117, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (118, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (119, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (120, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (121, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (122, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (123, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (124, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (125, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (126, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (127, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (128, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (129, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (130, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (131, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (132, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (133, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (134, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (135, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (136, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (137, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (138, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (139, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (140, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (141, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (142, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (143, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (144, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (145, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (146, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (147, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (148, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (149, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (150, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (151, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (152, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (153, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (154, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (155, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (156, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (157, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (158, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (159, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (160, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (161, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (162, 'page', 'GET', '/service/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (163, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (164, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (165, 'page', 'GET', '/service/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (166, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (167, 'page', 'GET', '/service/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (168, 'page', 'GET', '/service/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (169, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (170, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (171, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (172, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (173, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (174, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (175, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (176, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (177, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (178, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (179, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (180, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (181, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (182, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (183, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (184, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (185, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (186, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (187, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (188, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (189, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (190, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (191, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (192, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (193, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (194, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (195, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (196, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (197, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (198, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (199, '-1', 'DELETE', '/menu/-1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (200, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (201, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (202, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (203, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (204, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (205, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (206, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (207, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (208, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (209, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (210, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (211, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (212, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (213, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (214, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (215, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (216, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (217, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (218, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (219, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (220, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (221, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (222, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (223, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (224, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (225, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (226, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (227, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (228, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (229, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (230, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (231, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (232, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (233, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (234, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (235, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (236, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (237, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (238, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (239, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (240, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (241, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (242, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (243, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (244, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (245, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (246, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (247, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (248, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (249, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (250, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (251, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (252, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (253, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (254, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (255, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (256, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (257, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (258, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (259, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (260, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (261, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (262, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (263, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (264, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (265, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (266, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (267, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (268, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (269, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (270, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (271, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (272, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (273, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (274, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (275, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (276, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (277, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (278, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (279, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (280, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (281, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (282, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (283, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (284, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (285, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (286, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (287, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (288, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (289, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (290, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (291, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (292, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (293, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (294, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (295, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (296, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (297, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (298, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (299, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (300, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (301, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (302, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (303, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (304, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (305, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (306, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (307, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (308, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (309, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (310, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (311, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (312, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (313, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (314, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (315, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (316, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (317, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (318, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (319, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (320, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (321, '1', 'PUT', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (322, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (323, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (324, '1', 'PUT', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (325, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (326, '1', 'GET', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (327, '1', 'PUT', '/user/1', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (328, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (329, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (330, '2', 'GET', '/user/2', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (331, NULL, 'POST', '/user', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (332, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (333, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (334, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (335, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (336, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (337, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (338, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (339, 'page', 'GET', '/groupType/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (340, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (341, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (342, 'page', 'GET', '/service/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (343, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (344, 'page', 'GET', '/groupType/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (345, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (346, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (347, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (348, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (349, 'page', 'GET', '/groupType/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (350, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (351, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (352, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (353, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (354, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (355, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (356, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (357, 'front', 'GET', '/user/front/info', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (358, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (359, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (360, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (361, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (362, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (363, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (364, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (365, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (366, 'page', 'GET', '/groupType/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (367, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (368, 'page', 'GET', '/groupType/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (369, 'all', 'GET', '/groupType/all', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (370, 'page', 'GET', '/groupType/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (371, 'tree', 'GET', '/menu/tree', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (372, 'list', 'GET', '/element/list', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (373, 'page', 'GET', '/user/page', '2023-12-31 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (374, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (375, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (376, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (377, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (378, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (379, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (380, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (381, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (382, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (383, 'insertResume', 'POST', '/resume/insertResume', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (384, 'insertResume', 'POST', '/resume/insertResume', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (385, 'queryResumeByUsername', 'GET', '/resume/queryResumeByUsername', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (386, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (387, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (388, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (389, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (390, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (391, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (392, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (393, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (394, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (395, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (396, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (397, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (398, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (399, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (400, 'queryResumeByUsername', 'GET', '/resume/queryResumeByUsername', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (401, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (402, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (403, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (404, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (405, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (406, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (407, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (408, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (409, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (410, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (411, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (412, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (413, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (414, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (415, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (416, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (417, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (418, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (419, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (420, 'insertResume', 'POST', '/resume/insertResume', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (421, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (422, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (423, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (424, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (425, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (426, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (427, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (428, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (429, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (430, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (431, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (432, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (433, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (434, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (435, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (436, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (437, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (438, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (439, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (440, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (441, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (442, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (443, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (444, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (445, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (446, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (447, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (448, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (449, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (450, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (451, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (452, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (453, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (454, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (455, 'queryResumeById', 'GET', '/resume/queryResumeById/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (456, 'queryResumeById', 'GET', '/resume/queryResumeById/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (457, 'queryResumeById', 'GET', '/resume/queryResumeById/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (458, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (459, 'updateResumeById', 'PUT', '/resume/updateResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (460, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (461, 'queryResumeById', 'GET', '/resume/queryResumeById/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (462, 'insertResume', 'POST', '/resume/insertResume', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (463, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (464, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (465, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (466, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (467, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (468, 'page', 'GET', '/service/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (469, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (470, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (471, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (472, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (473, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (474, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (475, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (476, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (477, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (478, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (479, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (480, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (481, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (482, 'all', 'GET', '/groupType/all', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (483, 'page', 'GET', '/groupType/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (484, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (485, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (486, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (487, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (488, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (489, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (490, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (491, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (492, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (493, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (494, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (495, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (496, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (497, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (498, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (499, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (500, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (501, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (502, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (503, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (504, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (505, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (506, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (507, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (508, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (509, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (510, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (511, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (512, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (513, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (514, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (515, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (516, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (517, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (518, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (519, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (520, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (521, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (522, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (523, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (524, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (525, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (526, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (527, 'page', 'GET', '/groupType/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (528, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (529, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (530, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (531, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (532, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (533, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (534, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (535, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (536, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (537, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (538, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (539, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (540, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (541, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (542, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (543, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (544, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (545, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (546, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (547, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (548, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (549, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (550, '2', 'PUT', '/user/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (551, '2', 'PUT', '/user/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (552, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (553, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (554, '2', 'PUT', '/user/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (555, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (556, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (557, '6', 'PUT', '/user/6', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (558, '6', 'PUT', '/user/6', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (559, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (560, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (561, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (562, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (563, 'all', 'GET', '/groupType/all', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (564, 'page', 'GET', '/groupType/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (565, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (566, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (567, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (568, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (569, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (570, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (571, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (572, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (573, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (574, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (575, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (576, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (577, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (578, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (579, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (580, 'page', 'GET', '/groupType/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (581, 'all', 'GET', '/groupType/all', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (582, 'page', 'GET', '/groupType/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (583, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (584, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (585, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (586, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (587, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (588, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (589, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (590, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (591, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (592, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (593, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (594, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (595, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (596, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (597, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (598, '2', 'PUT', '/user/2', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (599, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (600, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (601, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (602, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (603, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (604, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (605, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (606, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (607, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (608, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (609, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (610, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (611, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (612, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (613, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (614, 'all', 'GET', '/groupType/all', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (615, 'page', 'GET', '/groupType/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (616, '1', 'GET', '/groupType/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (617, '1', 'GET', '/groupType/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (618, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (619, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (620, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (621, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (622, 'tree', 'GET', '/menu/tree', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (623, 'list', 'GET', '/element/list', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (624, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (625, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (626, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (627, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (628, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (629, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (630, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (631, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (632, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (633, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (634, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (635, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (636, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (637, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (638, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (639, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (640, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (641, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (642, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (643, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (644, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (645, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (646, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (647, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (648, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (649, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (650, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (651, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (652, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (653, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (654, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (655, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (656, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (657, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (658, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (659, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (660, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (661, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (662, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (663, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (664, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (665, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (666, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (667, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (668, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (669, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (670, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (671, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (672, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (673, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (674, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (675, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (676, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (677, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (678, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (679, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (680, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (681, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (682, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (683, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (684, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (685, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (686, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (687, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (688, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (689, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (690, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (691, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (692, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (693, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (694, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (695, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (696, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (697, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (698, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (699, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (700, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (701, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (702, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (703, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (704, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (705, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (706, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (707, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (708, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (709, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (710, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (711, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (712, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (713, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (714, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (715, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (716, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (717, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (718, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (719, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (720, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (721, '1', 'PUT', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (722, '1', 'PUT', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (723, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (724, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (725, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (726, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (727, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (728, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (729, '1', 'PUT', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (730, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (731, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (732, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (733, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (734, '1', 'PUT', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (735, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (736, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (737, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (738, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (739, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (740, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (741, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (742, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (743, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (744, '1', 'PUT', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (745, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (746, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (747, '1', 'PUT', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (748, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (749, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (750, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (751, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (752, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (753, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (754, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (755, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (756, '18', 'PUT', '/user/18', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (757, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (758, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (759, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (760, '1', 'GET', '/user/1', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (761, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (762, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (763, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (764, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (765, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (766, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (767, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (768, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (769, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (770, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (771, '18', 'GET', '/user/18', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (772, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (773, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (774, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (775, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (776, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (777, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (778, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (779, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (780, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (781, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (782, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (783, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (784, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (785, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (786, 'queryResumeById', 'GET', '/resume/queryResumeById/3', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (787, 'updateResumeById', 'PUT', '/resume/updateResumeById/3', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (788, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (789, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (790, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (791, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (792, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (793, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (794, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (795, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (796, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (797, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (798, 'queryResumeById', 'GET', '/resume/queryResumeById/3', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (799, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (800, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (801, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (802, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (803, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (804, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (805, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (806, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (807, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (808, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (809, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (810, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (811, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (812, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (813, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (814, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (815, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (816, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (817, '17', 'PUT', '/user/17', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (818, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (819, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (820, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (821, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (822, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (823, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (824, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (825, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (826, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (827, '22', 'GET', '/user/22', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (828, '22', 'PUT', '/user/22', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (829, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (830, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (831, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (832, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (833, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (834, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (835, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (836, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (837, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (838, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (839, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (840, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (841, 'insertResume', 'POST', '/resume/insertResume', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (842, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (843, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (844, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (845, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (846, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (847, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (848, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (849, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (850, '22', 'GET', '/user/22', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (851, '22', 'PUT', '/user/22', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (852, 'page', 'GET', '/user/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (853, '22', 'DELETE', '/user/22', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (854, '23', 'DELETE', '/user/23', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (855, 'checkUser', 'GET', '/user/checkUser', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (856, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (857, 'queryResumeById', 'GET', '/resume/queryResumeById/3', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (858, 'updateResumeById', 'PUT', '/resume/updateResumeById/3', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (859, 'page', 'GET', '/resume/page', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (860, 'deleteResume', 'DELETE', '/resume/deleteResume/4', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (861, 'front', 'GET', '/user/front/info', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');
INSERT INTO `gate_log` VALUES (862, 'pageByOrder', 'GET', '/gateLog/pageByOrder', '2024-01-01 00:00:00', 'system', 'system', '2.0.0.1', 'system');

SET FOREIGN_KEY_CHECKS = 1;
