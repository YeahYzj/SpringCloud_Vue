package com.github.wxiaoqi.security.common.aop;

import cn.hutool.core.net.NetUtil;
import com.github.wxiaoqi.security.common.vo.GateLog;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.Enumeration;
import java.util.UUID;

@Aspect
public class LoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);



    @Around("execution(public * com.github.wxiaoqi.security.*.rest.*.*(..))")
    public Object Around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();

        // 获取 HttpServletRequest
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String authorizationHeader = request.getHeader("Authorization");

        String requestMethod = request.getMethod();

        // 从请求路径中获取菜单名称，这里假设菜单名称在路径的第三段
        String[] pathSegments = request.getRequestURI().split("/");
        String menuName = null;
        if (pathSegments.length > 2) {
            menuName = pathSegments[2];
        }

        // 获取 URI
        String uri = request.getRequestURI();
        // 执行目标方法
        Object result = joinPoint.proceed();

        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;

        // 记录日志
        logger.info(joinPoint.getSignature().toShortString() + " executed in " + executionTime + "ms");
        GateLog gateLog = new GateLog();
        gateLog.setMenu(menuName).setCrtHost(NetUtil.getLocalhostStr()).setUri(uri).setOpt(requestMethod).setCrtName("system").setCrtUser("system").setBody("system");
        this.saveLog(gateLog);

        return result;
    }

    public int saveLog(GateLog gateLog) {
        int rowsAffected = 0;
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloud_admin_v1?useUnicode=true&characterEncoding=UTF8&useSSL=false&allowPublicKeyRetrieval=true", "root", "root");
            String sql = "INSERT INTO gate_log (menu, opt, uri,crt_time,crt_user,crt_name,crt_host,body) VALUES (?,?,?,?,?, ?, ?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // 设置参数
            preparedStatement.setString(1, gateLog.getMenu());
            preparedStatement.setString(2, gateLog.getOpt());
            preparedStatement.setString(3, gateLog.getUri());
            preparedStatement.setDate(4, new java.sql.Date(System.currentTimeMillis()));
            preparedStatement.setString(5, gateLog.getCrtUser());
            preparedStatement.setString(6,gateLog.getCrtName());
            preparedStatement.setString(7,gateLog.getCrtHost());
            preparedStatement.setString(8, gateLog.getBody());
            // 执行插入操作
            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rowsAffected;
    }

}
