package com.github.wxiaoqi.security.common.vo;

import lombok.Data;

@Data
public class RegisterReq {
    private String name;
    private String username;
    private String password;
    private Integer groupId;
    private int code = 200;
}
