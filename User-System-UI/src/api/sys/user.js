import request from '@/plugin/axios'

export function getUserPermissionInfo (token) {
  return request({
    url: '/api/admin/user/front/info?token=' + token,
    method: 'get'
  })
}
