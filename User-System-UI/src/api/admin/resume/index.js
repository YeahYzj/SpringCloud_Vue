import fetch from '@/plugin/axios'

export function page (query) {
  return fetch({
    url: '/api/admin/resume/page',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return fetch({
    url: '/api/admin/resume/insertResume',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return fetch({
    url: '/api/admin/resume/queryResumeById/' + id,
    method: 'get'
  })
}

export function delObj (id) {
  return fetch({
    url: '/api/admin/resume/deleteResume/' + id,
    method: 'delete'
  })
}

export function putObj (id, obj) {
  return fetch({
    url: '/api/admin/resume/updateResumeById/' + id,
    method: 'put',
    data: obj
  })
}


export function checkUser(token) {
  return fetch({
    url: '/api/admin/user/checkUser?number=2&token=' + token,
    method: 'get'
  })
}