import demo from './modules/demo'

import layoutHeaderAside from '@/layout/header-aside'
import layoutHeaderAsideMenuSide from '@/layout/header-aside/components/menu-side'

const meta = { requiresAuth: true }

/**
 * 在主框架内显示
 */
const frameIn = [
  {
    path: '/',
    redirect: { name: 'index' },
    component: layoutHeaderAside,
    children: [
      // 首页 必须 name:index
      {
        path: 'index',
        name: 'index',
        meta,
        component: () => import('@/pages/index')
      },
      // 刷新页面 必须保留
      {
        path: 'refresh',
        name: 'refresh',
        hidden: true,
        component: {
          beforeRouteEnter (to, from, next) {
            next(vm => vm.$router.replace(from.fullPath))
          },
          render: h => h()
        }
      }
    ]
  },

  // {
  //   name: "baseManager",
  //   path: "/admin",
  //   component: layoutHeaderAside,
  //   componentPath: "layout/header-aside/layout",
  //   meta: {
  //     "cache": true,
  //     "title": "基础配置管理"
  //   },
  //   children: [
  //     {
  //       name: "userManager",
  //       path: "/admin/user",
  //        component: ()=> import("@/pages/admin/user/index"),
  //       componentPath: "pages/admin/user/index",
  //       meta: {
  //         "cache": true,
  //         "title": "用户管理"
  //       },
  //       children: []
  //     },
  //     {
  //       name: "menuManager",
  //       path: "/admin/menu",
  //       component: ()=> import("@/pages/admin/menu/index"),
  //       componentPath: "pages/admin/menu/index",
  //       meta: {
  //         "cache": true,
  //         "title": "菜单管理"
  //       },
  //       children: []
  //     }
  //   ]
  // },
    // {
    //   path: '/admin/user',
    //   name: 'user-management',
    //   component: layoutHeaderAside,
    //   redirect: '/admin/user/user-list', // 添加重定向配置
    //   //component: () => import('@/pages/admin/user'), // Update this path
    //   meta: {
    //     title: '用户管理',
    //     icon: 'user'
    //     // Add other meta properties if needed
    //   },
    //   children: [
    //     {
    //       path: 'user-list',
    //       name: 'user-list',
    //       meta: {
    //         title: '用户列表'
    //       },
    //       component: () => import('@/pages/admin/user')
    //     }
    //   ]
    // },
  //
  // {
  //   path: '/admin/gateLog',
  //   name: 'file-archive-o',
  //   component: layoutHeaderAside,
  //   meta: {
  //     title: '菜单管理',
  //     icon: 'user'
  //     // Add other meta properties if needed
  //   },
  //   redirect: '/admin/gateLog/file-archive-o', // 添加重定向配置
  //   children: [
  //     {
  //       path: 'file-archive-o',
  //       name: 'file-archive-o',
  //       meta: {
  //         title: '菜单管理'
  //       },
  //       component: () => import('@/pages/admin/gateLog')
  //     }
  //   ]
  // },



  demo
]

/**
 * 在主框架之外显示
 */
const frameOut = [
  // 登录
  {
    path: '/login',
    name: 'login',
    component: () => import('@/pages/login')
  },

  {
    path: '/register',
    name: 'register',
    component: () => import('@/pages/register')
  }
]

/**
 * 错误页面
 */
const errorPage = [
  // 404
  {
    path: '*',
    name: '404',
    component: () => import('@/pages/error-page-404')
  }
]

// 导出需要显示菜单的
export const frameInRoutes = frameIn

// 重新组织后导出
export default [
  ...frameIn,
  ...frameOut,
  ...errorPage
]
