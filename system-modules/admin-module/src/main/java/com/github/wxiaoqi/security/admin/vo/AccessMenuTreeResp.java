package com.github.wxiaoqi.security.admin.vo;//package com.github.wxiaoqi.security.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author the sun
 * @create 2017-06-19 13:03
 */
@Data
public class AccessMenuTreeResp implements  Serializable{
    String title;
    String icon;
    String path;
    List<AccessMenuTreeResp> children=new ArrayList<>();


}
