package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.Group;
import com.github.wxiaoqi.security.admin.entity.GroupMember;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface GroupMemberMapper extends Mapper<GroupMember> {
    public void insertGroupMemberById (@Param("groupId") String groupId,@Param("userId") String userId);

    @Select("select distinct group_id from base_group_member where user_id = #{userId}")
    public List<String> queryGroupIdList(@Param("userId") String userId);
}