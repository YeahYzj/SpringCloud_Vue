package com.github.wxiaoqi.security.admin.vo;

import lombok.Data;


@Data
public class AccessInterface {
    String path;
    String method;
}
