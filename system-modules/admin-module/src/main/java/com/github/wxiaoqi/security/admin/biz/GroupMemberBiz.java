package com.github.wxiaoqi.security.admin.biz;

import com.ace.cache.annotation.CacheClear;
import com.github.wxiaoqi.security.admin.constant.AdminCommonConstant;
import com.github.wxiaoqi.security.admin.entity.Group;
import com.github.wxiaoqi.security.admin.entity.GroupMember;
import com.github.wxiaoqi.security.admin.entity.Menu;
import com.github.wxiaoqi.security.admin.entity.ResourceAuthority;
import com.github.wxiaoqi.security.admin.mapper.*;
import com.github.wxiaoqi.security.admin.vo.AuthorityMenuTree;
import com.github.wxiaoqi.security.admin.vo.GroupUsers;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;


@Service
@Transactional(rollbackFor = Exception.class)
public class GroupMemberBiz extends BaseBiz<GroupMemberMapper, GroupMember> {
    @Resource
    private GroupMemberMapper groupMemberMapper;


    public void addGroupMember(String groupId, String userId) {
        groupMemberMapper.insertGroupMemberById(groupId, userId);
    }

    public List<String> selectGroupUsers(String groupId) {
        List<String> groupIds = groupMemberMapper.queryGroupIdList(groupId);
        return groupIds;
    }

}
