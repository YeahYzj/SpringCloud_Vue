package com.github.wxiaoqi.security.admin.vo;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ace on 2017/8/22.
 */
@Data
public class FrontUserV2 implements Serializable {
    /**
     * 头像地址
     */
    public String avatarUrl;
    /**
     * 昵称
     */
    public String userName;
    /**
     * 角色列表
     */
    public List<String> userRoles;
    /**
     * 用户动作权限编码
     */
    private List<String> userPermissions = new ArrayList<>();
    /**
     * 可访问菜单
     */
    //private List<AccessMenuTree> accessMenus = new ArrayList<>();
    private JSONArray accessMenus = new JSONArray();



    //private List<AccessMenuTree> accessHeader = new ArrayList<>();
    private List<HeaderMenu> accessHeader = new ArrayList<>();
    /**
     * 可访问路由
     */
    //private List<AccessRouteTree> accessRoutes = new ArrayList<>();
    private List<AccessRouteTreeNew> accessRoutes = new ArrayList<>();


    /**
     * 可访问接口
     */
    private List<AccessInterface> accessInterfaces = new ArrayList<>();

    private String isAdmin = "0";

}
