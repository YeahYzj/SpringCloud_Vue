package com.github.wxiaoqi.security.admin.rest;

import com.github.wxiaoqi.security.admin.biz.GroupMemberBiz;
import com.github.wxiaoqi.security.admin.biz.MenuBiz;
import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.entity.Menu;
import com.github.wxiaoqi.security.admin.entity.Resume;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.rpc.service.PermissionService;
import com.github.wxiaoqi.security.admin.rpc.service.ResumeService;
import com.github.wxiaoqi.security.admin.vo.FrontUser;
import com.github.wxiaoqi.security.admin.vo.FrontUserV2;
import com.github.wxiaoqi.security.admin.vo.FrontUserVo;
import com.github.wxiaoqi.security.admin.vo.MenuTree;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.auth.common.util.jwt.IJWTInfo;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("user")
public class UserController extends BaseController<UserBiz, User> {
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private MenuBiz menuBiz;

    @Resource
    private UserAuthUtil userAuthUtil;

    @Resource
    private GroupMemberBiz groupMemberBiz;

    @Resource
    private ResumeService resumeService;


    @RequestMapping(value = "/front/info", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<?> getUserInfo(String token) throws Exception {
        FrontUserV2 userInfo = permissionService.getUserInfoV2(token);
        if (userInfo == null) {
            return new ObjectRestResponse<>().data(null);
        } else {
            return new ObjectRestResponse<>().data(userInfo);
        }
    }

    @RequestMapping(value = "/front/menus", method = RequestMethod.GET)
    public @ResponseBody
    List<MenuTree> getMenusByUsername(String token) throws Exception {
        return permissionService.getMenusByUsername(token);
    }

    @RequestMapping(value = "/front/menu/all", method = RequestMethod.GET)
    public @ResponseBody
    List<Menu> getAllMenus() throws Exception {
        return menuBiz.selectListAll();
    }


    @SneakyThrows
    @GetMapping(value = "/checkUser")
    public ObjectRestResponse<?> checkUser(@RequestParam Integer number,@RequestParam String token) {
        Map<String, Object> map = new HashMap<>();
        IJWTInfo info = userAuthUtil.getInfoFromToken(token);
        List<String> groupIds = groupMemberBiz.selectGroupUsers(info.getId());
        if (groupIds.contains("6")) { //普通用户
            Example example = new Example(User.class);
            example.createCriteria().andEqualTo("id", info.getId());

            if (number == 1) {
                List<User>  users= baseBiz.selectByExample(example);
                map.put("users", users);
                map.put("isShow", false);
            } else if (number == 2) {
                List<Resume> resumes = resumeService.selectResumeByUserId(info.getId());
                map.put("resumes", resumes);
                if (resumes.size() >= 1) {
                    map.put("isShow", false);
                }else {
                    map.put("isShow", true);
                }
            }
        }else {
            map.put("isShow", true);
            map.put("list", null);
        }
        return new ObjectRestResponse<>().data(map);
    }
}
