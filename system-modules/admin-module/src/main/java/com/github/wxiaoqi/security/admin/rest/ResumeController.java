package com.github.wxiaoqi.security.admin.rest;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.admin.entity.Resume;
import com.github.wxiaoqi.security.admin.rpc.service.ResumeService;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.auth.common.util.jwt.IJWTInfo;
import com.github.wxiaoqi.security.common.context.BaseContextHandler;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import javax.swing.text.html.parser.Entity;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/resume")
public class ResumeController {

    @Autowired
    private ResumeService resumeService;
    @Autowired
    private UserAuthUtil userAuthUtil;

    @GetMapping(value = "/queryResumeById/{id}")
    public ObjectRestResponse<?> getUserInfo( @PathVariable("id") Long id) throws Exception {

        //String username = userAuthUtil.getInfoFromToken(token).getUniqueName();

        return new ObjectRestResponse<>().data(resumeService.selectResumeById(id));
    }

    @PutMapping(value = "/updateResumeById/{id}")
    public ObjectRestResponse<?> updateResumeById(@PathVariable("id") Long id, @RequestBody Resume resume) throws Exception {
        IJWTInfo infoFromToken = userAuthUtil.getInfoFromToken(resume.getUserId());
        resume.setId(id);
        resume.setUserId(infoFromToken.getId());
        resumeService.updateResume(resume);
        return new ObjectRestResponse<>();
    }

    @PostMapping(value = "/insertResume")
    public ObjectRestResponse<?> insertResume(@RequestBody Resume resume) throws Exception {
        IJWTInfo infoFromToken = userAuthUtil.getInfoFromToken(resume.getUserId());
        resume.setUserId(infoFromToken.getId());
        resumeService.insertResume(resume);
        return new ObjectRestResponse<>();
    }

    @DeleteMapping(value = "/deleteResume/{id}")
    public ObjectRestResponse<?> deleteResume(String token, @PathVariable("id") Long id) throws Exception {
        //String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        resumeService.deleteResume(id);
        return new ObjectRestResponse<>();
    }

    @GetMapping(value = "/page")
    public TableResultResponse<Resume> queryResumeByUsername(@RequestParam Map<String, Object> params) throws Exception {
        Query query = new Query(params);
        Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
        List<Resume> resumes = resumeService.selectResumeByUsername();
        return new TableResultResponse<Resume>(result.getTotal(), resumes);
    }
}
