package com.github.wxiaoqi.security.admin.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author the sun
 * @create 2017-06-19 13:03
 */
@Data
public class AccessRouteTreeNew implements Serializable {
    String name;
    //String icon;
    String path;
    String component;
    String componentPath;
    /**
     * "meta": {
     * "title": "系统设置",
     * "cache": true
     * }
     */
    JSONObject meta;

    List<AccessRouteTreeNew> children = new ArrayList<AccessRouteTreeNew>();


}
