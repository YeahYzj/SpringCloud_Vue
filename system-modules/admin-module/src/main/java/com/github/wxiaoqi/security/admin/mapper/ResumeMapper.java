package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.Resume;
import com.github.wxiaoqi.security.admin.entity.User;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface ResumeMapper extends Mapper<Resume> {

    @Select("select * from base_resume where user_id = #{userId}")
    public List<Resume> selectResumeByUserId(String userId);

}
