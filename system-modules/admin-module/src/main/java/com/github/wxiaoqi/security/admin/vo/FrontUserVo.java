package com.github.wxiaoqi.security.admin.vo;

import com.github.wxiaoqi.security.api.vo.authority.PermissionInfo;
import lombok.Data;

import java.util.List;

/**
 * Created by ace on 2017/8/22.
 */
@Data
public class FrontUserVo {
    private String userName;
    private List<String> userRoles;
    private List<String> userPermissions;
    private List<Menu> accessMenus;
    private List<Route> accessRoutes;
    private List<AccessInterface> accessInterfaces;
    private int isAdmin;
    private String avatarUrl;


    @Data
    public static class Menu {
        private String title;
        private String path;
        private String icon;
        private List<Menu> children;
    }
    @Data
    public static class Route {
        private String name;
        private String path;
        private String component;
        private String componentPath;
        private Meta meta;
        private List<Route> children;
    }
    @Data
    public static class AccessInterface {
        private String path;
        private String method;
    }
    @Data
    public static class Meta {
        private String title;
        private boolean cache;
    }

}
