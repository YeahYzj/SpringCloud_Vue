package com.github.wxiaoqi.security.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "base_resume")
public class Resume {
    // 个人信息
    @Id
    private Long id;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date birthDate;
    private String contactNumber;
    private String address;

    // 教育经历
    private String educationDegree;
    private String almaMater;
    private String major;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date graduationDate;

    // 工作经历
    private String jobTitle;
    private String companyName;
    private String jobResponsibility;
    private String employmentPeriod;

    // 技能和证书
    private String skills;
    private String professionalCertificate;
    private String trainingExperience;

    // 项目经验
    private String projectName;
    private String role;
    private String projectDescription;
    private String projectAchievement;

    // 奖项和荣誉
    private String awards;
    private String honors;

    // 其他信息
    private String hobbies;
    private String socialMediaAccounts;
    private String selfEvaluation;

    private String userId;

    // 构造函数、Getter 和 Setter 方法省略
}
