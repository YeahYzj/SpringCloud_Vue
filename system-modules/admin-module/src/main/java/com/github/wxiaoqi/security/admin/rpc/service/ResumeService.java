package com.github.wxiaoqi.security.admin.rpc.service;

import com.github.wxiaoqi.security.admin.entity.Resume;
import com.github.wxiaoqi.security.admin.mapper.ResumeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ResumeService {

    @Resource
    private ResumeMapper resumeMapper;

    public void insertResume(Resume resume) {
        resumeMapper.insert(resume);
    }

    public Resume selectResumeById(Long id) {
        return resumeMapper.selectByPrimaryKey(id);
    }

    public void updateResume(Resume resume) {
        resumeMapper.updateByPrimaryKeySelective(resume);
    }

    public void deleteResume(Long id) {
        resumeMapper.deleteByPrimaryKey(id);
    }

    public List<Resume> selectResumeByUsername() {
        return resumeMapper.selectAll();
    }

    public List<Resume> selectResumeByUserId(String userId) {
        return resumeMapper.selectResumeByUserId(userId);
    }
}
