package com.github.wxiaoqi.security.admin.rpc.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.wxiaoqi.security.admin.biz.ElementBiz;
import com.github.wxiaoqi.security.admin.biz.GroupBiz;
import com.github.wxiaoqi.security.admin.biz.MenuBiz;
import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.constant.AdminCommonConstant;
import com.github.wxiaoqi.security.admin.entity.*;
import com.github.wxiaoqi.security.admin.mapper.GroupMemberMapper;
import com.github.wxiaoqi.security.admin.vo.*;
import com.github.wxiaoqi.security.api.vo.authority.PermissionInfo;
import com.github.wxiaoqi.security.api.vo.user.UserInfo;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.common.constant.CommonConstants;
import com.github.wxiaoqi.security.common.util.TreeUtil;
import com.github.wxiaoqi.security.common.vo.RegisterReq;
import com.github.wxiaoqi.security.common.vo.TreeNode;
import io.swagger.models.auth.In;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class PermissionService {
    @Autowired
    private UserBiz userBiz;
    @Autowired
    private MenuBiz menuBiz;
    @Autowired
    private ElementBiz elementBiz;
    @Resource
    private GroupMemberMapper groupMemberMapper;
    @Autowired
    private UserAuthUtil userAuthUtil;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);


    public UserInfo getUserByUsername(String username) {
        UserInfo info = new UserInfo();
        User user = userBiz.getUserByUsername(username);
        BeanUtils.copyProperties(user, info);
        info.setId(user.getId().toString());
        return info;
    }

    public UserInfo validate(String username,String password){
        UserInfo info = new UserInfo();
        User user = userBiz.getUserByUsername(username);
//        if (encoder.matches(password, user.getPassword())) {
//            BeanUtils.copyProperties(user, info);
//            info.setId(user.getId().toString());
//        }

        try {
            BeanUtils.copyProperties(user, info);
            info.setId(user.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return info;
    }

    public RegisterReq register(RegisterReq req){
        User user = new User();
        user.setUsername(req.getUsername());
        user.setPassword(req.getPassword());
        user.setName(req.getName());
        userBiz.insertSelective(user);
        User bizUser = userBiz.getUserByUsername(user.getUsername());
        groupMemberMapper.insertGroupMemberById(String.valueOf(req.getGroupId()),String.valueOf(bizUser.getId()));
        return req;
    }

    public List<PermissionInfo> getAllPermission() {
        List<Menu> menus = menuBiz.selectListAll();
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        menu2permission(menus, result);
        List<Element> elements = elementBiz.getAllElementPermissions();
        element2permission(result, elements);
        return result;
    }

    private void menu2permission(List<Menu> menus, List<PermissionInfo> result) {
        PermissionInfo info;
        for (Menu menu : menus) {
            if (StringUtils.isBlank(menu.getHref())) {
                menu.setHref("/" + menu.getCode());
            }
            info = new PermissionInfo();
            info.setCode(menu.getCode());
            info.setType(AdminCommonConstant.RESOURCE_TYPE_MENU);
            info.setName(AdminCommonConstant.RESOURCE_ACTION_VISIT);
            String uri = menu.getHref();
            if (!uri.startsWith("/")) {
                uri = "/" + uri;
            }
            info.setUri(uri);
            info.setMethod(AdminCommonConstant.RESOURCE_REQUEST_METHOD_GET);
            result.add(info
            );
            info.setMenu(menu.getTitle());
        }
    }

    public List<PermissionInfo> getPermissionByUsername(String username) {
        User user = userBiz.getUserByUsername(username);
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getId());
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        menu2permission(menus, result);
        List<Element> elements = elementBiz.getAuthorityElementByUserId(user.getId() + "");
        element2permission(result, elements);
        return result;
    }

    private void element2permission(List<PermissionInfo> result, List<Element> elements) {
        PermissionInfo info;
        for (Element element : elements) {
            info = new PermissionInfo();
            info.setCode(element.getCode());
            info.setType(element.getType());
            info.setUri(element.getUri());
            info.setMethod(element.getMethod());
            info.setName(element.getName());
            info.setMenu(element.getMenuId());
            result.add(info);
        }
    }


    private List<MenuTree> getMenuTree(List<Menu> menus, int root) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        for (Menu menu : menus) {
            node = new MenuTree();
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees, root);
    }

    public FrontUserVo getUserInfo(String token) throws Exception {
        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        UserInfo user = this.getUserByUsername(username);
        FrontUserVo frontUser = new FrontUserVo();
        frontUser.setUserName(username);
        if (username.contains("admin")) {
            frontUser.setIsAdmin(1);
        }else {
            frontUser.setIsAdmin(0);
        }
        frontUser.setUserRoles(Arrays.asList("R_MENUADMIN"));
        frontUser.setUserPermissions(Arrays.asList("p_menu_view","p_menu_edit","p_menu_menu","p_menu_delete","p_menu_add"));
        BeanUtils.copyProperties(user, frontUser);
        List<PermissionInfo> permissionInfos = this.getPermissionByUsername(username);
        Stream<PermissionInfo> menus = permissionInfos.parallelStream().filter((permission) -> {
            return permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        String menusStr = "[\n" +
                "      {\n" +
                "        \"title\": \"系统\",\n" +
                "        \"path\": \"/system\",\n" +
                "        \"icon\": \"cogs\",\n" +
                "        \"children\": [\n" +
                "          {\n" +
                "            \"title\": \"系统设置\",\n" +
                "            \"icon\": \"cogs\",\n" +
                "            \"children\": [\n" +
                "              {\n" +
                "                \"title\": \"菜单管理\",\n" +
                "                \"path\": \"/system/menu\",\n" +
                "                \"icon\": \"th-list\"\n" +
                "              }\n" +
                "            ]\n" +
                "          },\n" +
                "          {\n" +
                "            \"title\": \"组织架构\",\n" +
                "            \"icon\": \"pie-chart\",\n" +
                "            \"children\": [\n" +
                "              {\n" +
                "                \"title\": \"部门管理\",\n" +
                "                \"icon\": \"html5\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"title\": \"职位管理\",\n" +
                "                \"icon\": \"opencart\"\n" +
                "              }\n" +
                "            ]\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]";
        List<FrontUserVo.Menu> menusList = JSONObject.parseArray(menusStr, FrontUserVo.Menu.class);
        frontUser.setAccessMenus(menusList);
        //frontUser.setMenus(menus.collect(Collectors.toList()));
        Stream<PermissionInfo> elements = permissionInfos.parallelStream().filter((permission) -> {
            return !permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setAvatarUrl("https://api.adorable.io/avatars/85/abott@adorable.png");
        String accessRoutes = "[\n" +
                "      {\n" +
                "        \"name\": \"System\",\n" +
                "        \"path\": \"/system\",\n" +
                "        \"component\": \"layoutHeaderAside\",\n" +
                "        \"componentPath\": \"layout/header-aside/layout\",\n" +
                "        \"meta\": {\n" +
                "          \"title\": \"系统设置\",\n" +
                "          \"cache\": true\n" +
                "        },\n" +
                "        \"children\": [\n" +
                "          {\n" +
                "            \"name\": \"MenuPage\",\n" +
                "            \"path\": \"/system/menu\",\n" +
                "            \"component\": \"menu\",\n" +
                "            \"componentPath\": \"pages/sys/menu/index\",\n" +
                "            \"meta\": {\n" +
                "              \"title\": \"菜单管理\",\n" +
                "              \"cache\": true\n" +
                "            }\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"RoutePage\",\n" +
                "            \"path\": \"/system/route\",\n" +
                "            \"component\": \"route\",\n" +
                "            \"componentPath\": \"pages/sys/route/index\",\n" +
                "            \"meta\": {\n" +
                "              \"title\": \"路由管理\",\n" +
                "              \"cache\": true\n" +
                "            }\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"RolePage\",\n" +
                "            \"path\": \"/system/role\",\n" +
                "            \"component\": \"role\",\n" +
                "            \"componentPath\": \"pages/sys/role/index\",\n" +
                "            \"meta\": {\n" +
                "              \"title\": \"角色管理\",\n" +
                "              \"cache\": true\n" +
                "            }\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"UserPage\",\n" +
                "            \"path\": \"/system/user\",\n" +
                "            \"component\": \"user\",\n" +
                "            \"componentPath\": \"pages/sys/user/index\",\n" +
                "            \"meta\": {\n" +
                "              \"title\": \"用户管理\",\n" +
                "              \"cache\": true\n" +
                "            }\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"InterfacePage\",\n" +
                "            \"path\": \"/system/interface\",\n" +
                "            \"component\": \"interface\",\n" +
                "            \"meta\": {\n" +
                "              \"title\": \"接口管理\"\n" +
                "            }\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]";
        List<FrontUserVo.Route> routes = JSONObject.parseArray(accessRoutes, FrontUserVo.Route.class);
        frontUser.setAccessRoutes(routes);
        String accessInterfaces = "[\n" +
                "      {\n" +
                "        \"path\": \"/menu/:id\",\n" +
                "        \"method\": \"get\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"path\": \"/menu\",\n" +
                "        \"method\": \"get\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"path\": \"/menu/save\",\n" +
                "        \"method\": \"post\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"path\": \"/interface/paged\",\n" +
                "        \"method\": \"get\"\n" +
                "      }\n" +
                "    ]";
        List<FrontUserVo.AccessInterface> accessInterfaceList = JSONObject.parseArray(accessInterfaces, FrontUserVo.AccessInterface.class);
        frontUser.setAccessInterfaces(accessInterfaceList);
        //frontUser.setElements(elements.collect(Collectors.toList()));
        return frontUser;
    }


    @SneakyThrows
    public FrontUserV2 getUserInfoV2(String token) {
        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        User user = userBiz.getUserByUsername(username);
        FrontUserV2 frontUser = new FrontUserV2();
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getId());
        List<AccessMenuTree> menuTrees = new ArrayList<AccessMenuTree>();
        List<AccessRouteTree> routeTrees = new ArrayList<>();
        AccessMenuTree node = null;
        HeaderMenu headerMenu = null;
        AccessRouteTree routeNode = null;
        List<Integer> menuIds = new ArrayList<>();
        List<HeaderMenu> header = new ArrayList<>();
        // 设置菜单树
        for (Menu menu : menus) {
            node = new AccessMenuTree();
            node.setIcon(menu.getIcon());
            node.setPath(menu.getHref());
            node.setTitle(menu.getTitle());
            node.setId(menu.getId());
            node.setParentId(menu.getParentId());
            menuTrees.add(node);
            // 设置系统头部菜单
            if (menu.getParentId().equals(AdminCommonConstant.ROOT)) {
                menuIds.add(menu.getId());
//                node = new AccessMenuTree();
//                node.setIcon(menu.getIcon());
//                node.setPath(menu.getHref());
//                node.setTitle(menu.getTitle());
//                node.setId(menu.getId());
//                node.setParentId(menu.getParentId());
//                header.add(node);
                headerMenu = new HeaderMenu();
                headerMenu.setIcon(menu.getIcon());
                headerMenu.setPath(menu.getHref());
                headerMenu.setTitle(menu.getTitle());
                header.add(headerMenu);
                continue;
            }
            routeNode = new AccessRouteTree();
            routeNode.setIcon(menu.getIcon());
            routeNode.setId(menu.getId());
            routeNode.setPath(menu.getHref());
            routeNode.setParentId(menu.getParentId());
            routeNode.setName(menu.getCode());
            routeNode.setComponent(menu.getAttr3());
            routeNode.setComponentPath(menu.getAttr1());
            JSONObject jsonObject = JSONObject.parseObject(menu.getAttr2());
            jsonObject.put("title", menu.getTitle());
            routeNode.setMeta(jsonObject);
            routeTrees.add(routeNode);
        }
        // 配置页面资源权限
        List<Element> elements = elementBiz.getAuthorityElementByUserId(user.getId() + "");
        List<String> permissions = new ArrayList<>();
        List<AccessInterface> interfaces = new ArrayList<>();
        AccessInterface accessInterface = null;
        for (Element element : elements) {
            accessInterface = new AccessInterface();
            permissions.add(element.getCode());
            accessInterface.setMethod(element.getMethod());
            accessInterface.setPath(element.getUri());
            interfaces.add(accessInterface);
        }
        // 配置路由权限
        List<AccessRouteTree> accessRoutes = new ArrayList<>();
        for (Integer menuId : menuIds) {
            accessRoutes.addAll(TreeUtil.bulid(routeTrees, menuId));
        }


        List<AccessMenuTree> treeList = TreeUtil.bulid(menuTrees, AdminCommonConstant.ROOT);
        List<AccessMenuTreeResp> accessMenuTreeResps = new ArrayList<>();
//        for (AccessMenuTree accessMenuTree : treeList) {
//            AccessMenuTreeResp accessMenuTreeResp = new AccessMenuTreeResp();
//            BeanUtils.copyProperties(accessMenuTree,accessMenuTreeResp);
//            List<AccessMenuTreeResp> children = new ArrayList<AccessMenuTreeResp>();
//            for (TreeNode child : accessMenuTree.getChildren()) {
//                child = (AccessMenuTree) child;
//                AccessMenuTreeResp c = new AccessMenuTreeResp();
//                BeanUtils.copyProperties(child,c);
//                children.add(c);
//            }
//            accessMenuTreeResp.setChildren(children);
//            accessMenuTreeResps.add(accessMenuTreeResp);
//        }

        menuTrees = menuTrees.stream().filter(item -> item.getParentId() == -1).collect(Collectors.toList());
        for (AccessMenuTree menuTree : menuTrees) {
            AccessMenuTreeResp accessMenuTreeResp = copyMenuTree(menuTree);
            accessMenuTreeResps.add(accessMenuTreeResp);
        }
        JSONArray jsonArray = JSONArray.parseArray(JSONObject.toJSONString(accessMenuTreeResps));
        // 处理 JSON 数组
        handleJSONArray(jsonArray);

        frontUser.setAccessMenus(jsonArray);

        frontUser.setAccessHeader(header);

        List<AccessRouteTreeNew> accessRouteTreeNewList = new ArrayList<>();
        for (AccessRouteTree accessRoute : accessRoutes) {
            AccessRouteTreeNew accessRouteNew = copyAccessRouteTree(accessRoute);
            accessRouteTreeNewList.add(accessRouteNew);
        }

        frontUser.setAccessRoutes(accessRouteTreeNewList);


        frontUser.setUserPermissions(permissions);
        frontUser.setUserName(user.getName());
        frontUser.setAccessInterfaces(interfaces);
        //TODO 待接入头像附件上传服务
        frontUser.setAvatarUrl("https://api.adorable.io/avatars/85/abott@adorable.png");
        return frontUser;
    }

    private static void handleJSONArray(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            Object obj = jsonArray.get(i);
            if (obj instanceof JSONObject) {
                // 处理 JSON 对象
                handleJSONObject((JSONObject) obj);
            } else if (obj instanceof JSONArray) {
                // 递归处理嵌套的 JSON 数组
                handleJSONArray((JSONArray) obj);
            }
        }
    }

    private static void handleJSONObject(JSONObject jsonObject) {
        // 检查是否有 children 属性
        if (jsonObject.containsKey("children")) {
            // 获取 children 属性值
            JSONArray children = jsonObject.getJSONArray("children");
            // 如果 children 是空数组，移除该属性
            if (children != null && children.isEmpty()) {
                jsonObject.remove("children");
            } else {
                // 递归处理 children 属性
                handleJSONArray(children);
            }
        }
    }


    private static AccessMenuTreeResp copyMenuTree(TreeNode source) {
        AccessMenuTreeResp destination = new AccessMenuTreeResp();
        BeanUtils.copyProperties(source, destination);

        if (source.getChildren() != null && !source.getChildren().isEmpty()) {
            List<AccessMenuTreeResp> children = new ArrayList<>();
            for (TreeNode child : source.getChildren()) {
                AccessMenuTreeResp childResp = copyMenuTree(child);
                children.add(childResp);
            }
            destination.setChildren(children);
        }

        return destination;
    }


    private static AccessRouteTreeNew copyAccessRouteTree(TreeNode source) {
        AccessRouteTreeNew accessRouteTreeNew = new AccessRouteTreeNew();
        BeanUtils.copyProperties(source, accessRouteTreeNew);

        if (source.getChildren() != null && !source.getChildren().isEmpty()) {
            List<AccessRouteTreeNew> children = new ArrayList<>();
            for (TreeNode child : source.getChildren()) {
                AccessRouteTreeNew childResp = copyAccessRouteTree(child);
                children.add(childResp);
            }
            accessRouteTreeNew.setChildren(children);
        }

        return accessRouteTreeNew;
    }



    public List<MenuTree> getMenusByUsername(String token) throws Exception {
        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        User user = userBiz.getUserByUsername(username);
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getId());
        return getMenuTree(menus,AdminCommonConstant.ROOT);
    }
}
