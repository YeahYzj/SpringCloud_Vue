package com.github.wxiaoqi.security.auth.service.impl;

import com.github.wxiaoqi.security.api.vo.user.UserInfo;
import com.github.wxiaoqi.security.auth.common.util.jwt.JWTInfo;
import com.github.wxiaoqi.security.auth.feign.IUserService;
import com.github.wxiaoqi.security.auth.service.AuthService;
import com.github.wxiaoqi.security.auth.util.user.JwtAuthenticationRequest;
import com.github.wxiaoqi.security.auth.util.user.JwtTokenUtil;
import com.github.wxiaoqi.security.common.exception.auth.UserInvalidException;
import com.github.wxiaoqi.security.common.vo.RegisterReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {

    private JwtTokenUtil jwtTokenUtil;
    private IUserService userService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    public AuthServiceImpl(
            JwtTokenUtil jwtTokenUtil,
            IUserService userService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }

    @Override
    public Map<String,Object> login(JwtAuthenticationRequest authenticationRequest) throws Exception {
        Map<String, Object> map = new HashMap<>();
        UserInfo info = userService.validate(authenticationRequest);
        if (info.getLogout()) {
            throw new UserInvalidException("账号已注销！");
        }
        if (!StringUtils.isEmpty(info.getId())) {
            String token = jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getId() + "", info.getName()));
            map.put("data", token);
            map.put("id", info.getId());
            map.put("name", info.getName());
            return map;
        }
        throw new UserInvalidException("用户不存在或账户密码错误!");
    }

    @Override
    public void validate(String token) throws Exception {
        jwtTokenUtil.getInfoFromToken(token);
    }

    @Override
    public String refresh(String oldToken) throws Exception {
        return jwtTokenUtil.generateToken(jwtTokenUtil.getInfoFromToken(oldToken));
    }

    @Override
    public void logout(String token) throws Exception {
        /*懒人做法直接移除cookie，否则参考最新版本的移除方式*/
//        IJWTInfo infoFromToken = jwtTokenUtil.getInfoFromToken(token);
//        String tokenId = infoFromToken.getTokenId();
//        stringRedisTemplate.delete(RedisKeyConstant.REDIS_KEY_TOKEN + ":" + tokenId);
//        stringRedisTemplate.opsForZSet().remove(RedisKeyConstant.REDIS_KEY_TOKEN, tokenId);
    }

    @Override
    public RegisterReq register(RegisterReq req) throws Exception {
        return userService.register(req);
    }

}
