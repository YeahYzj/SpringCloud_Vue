package com.github.wxiaoqi.security.auth.service;


import com.github.wxiaoqi.security.auth.util.user.JwtAuthenticationRequest;
import com.github.wxiaoqi.security.common.vo.RegisterReq;

import java.util.Map;

public interface AuthService {
    Map<String,Object> login(JwtAuthenticationRequest authenticationRequest) throws Exception;
    String refresh(String oldToken) throws Exception;
    void validate(String token) throws Exception;

    void logout(String token) throws Exception;

    RegisterReq register(RegisterReq req) throws Exception;
}
