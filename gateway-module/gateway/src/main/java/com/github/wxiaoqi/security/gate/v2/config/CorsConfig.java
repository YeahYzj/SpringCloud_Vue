package com.github.wxiaoqi.security.gate.v2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CorsConfig {

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();

        // 允许跨域的来源，这里是允许所有来源，你也可以指定特定的域名
        config.addAllowedOrigin("*");

        // 允许跨域的请求方法，如 GET、POST 等
        config.addAllowedMethod("*");

        // 允许携带cookie信息
        config.setAllowCredentials(true);

        // 允许跨域的请求头
        config.addAllowedHeader("*");

        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}

